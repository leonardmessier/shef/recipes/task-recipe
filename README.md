# Task recipe

Shef recipe for task and time tracking

## Configuration

### Taskwarrior

Create the `<HOME>/Workspace/<CLIENT>/.config/task/tasrc` file with following content :

```dosini
data.location=<HOME>/Workspace/<CLIENT>/.local/share/task
```

### Bugwarrior

Create the `<HOME>/Workspace/<CLIENT>/.config/task/bugwarriorrc` file with the following content :

```dosini
[general]

taskrc = <HOME>/Workspace/<CLIENT>/.config/task/taskrc
annotation_links = False
annotation_comments = False
log.level = DEBUG
log.file = <HOME>/.local/var/log/bugwarrior.log
targets = redmine,gitlab

[redmine]
service = redmine
redmine.url = <REDMINE_URL>
redmine.key = <REDMINE_KEY>
redmine.project_template = {%% if redmineprojectname == "" or redmineprojectname == "" or redmineprojectname == "" %%}<PROJECT1>{%% elif redmineprojectname == "" %%}<PROJECT2>{%% endif %%}
redmine.query = limit=100&project_id=<PROJECT_ID>&subproject_id=!*&assigned_to_id=me
redmine.description_template = #{{redmineid}}: {{redminesubject}}
redmine.add_tags = ISSUE

[gitlab]
service = gitlab
gitlab.login = <GITLAB_LOGIN>
gitlab.token = <GITLAB_TOKEN>
gitlab.host = <GITLAB_HOST>
gitlab.include_regex = <GITLAB_REPO_REGEX> <GITLAB_CLIENT_GROUP>/<GITLAB_PROJECT_GROUPS>/*
gitlab.description_template = !{{gitlabnumber}}: {{gitlabtitle}}
gitlab.project_template = <PROJECT>
gitlab.add_tags = repo:{{ gitlabrepo }},MR
```

### Direnv

Set `TASKRC` variable and `TASK_FILTER` variables

```
# Taskwarrior
# -----------

export TASKRC="<HOME>/Workspace/<CLIENT>/.config/task/taskrc"
export TASK_FILTER=project:<PROJECT>

# Bugwarrior
# ----------

export BUGWARRIORRC=<HOME>/Workspace/<CLIENT>/.config/task/bugwarriorrc
```
